i=1;
j=1;
image = zeros(1944,2592,3);
%red squares
for i = 1:2:30 
    for j = 1:2:30
        image((10*(i-1)+1):((10*(i-1))+10),(10*(j-1)+1:((10*(j-1))+10)),1)=1;
    end
end
%green squares
for i = 2:2:30
    for j = 2:2:30
        image(((10*(i-1))+1):((10*(i-1))+10),((10*(j-1))+1:((10*(j-1))+10)),2)=1;
    end
end
%blue squares
for i = 2:2:30 
    for j = 1:2:30
        image((10*(i-1)+1):((10*(i-1))+10),(10*(j-1)+1:((10*(j-1))+10)),3)=1;
    end
end

%coloured ribbons that cross
image(401:410,:,1)=1;
image(411:420,:,2)=1;
image(421:430,:,3)=1;

image(:,401:410,1)=1;
image(:,411:420,2)=1;
image(:,421:430,3)=1;

%intersecting squares
image(431:490,431:490,1)=1;
image(471:530,431:490,2)=1;
image(431:490,471:530,3)=1;

%graded square black to white
for i = 1:100
    for j = 1:100
    image(i+480,j,1)=((i+j)/2)/100;
    image(i+480,j,2)=((i+j)/2)/100;
    image(i+480,j,3)=((i+j)/2)/100;
    end
end

%graded squares of different colours
for i = 1:100
    for j = 1:100
    image(i+480,j+100,1)=((i+j)/2)/100;
    image(i+480,j+100,2)=(100-((i+j)/2))/100;
    end
end

for i = 1:100
    for j = 1:100
    image(i+580,j+100,3)=((i+j)/2)/100;
    image(i+580,j+100,2)=(100-((i+j)/2))/100;
    end
end

for i = 1:100
    for j = 1:100
    image(i+580,j,3)=((i+j)/2)/100;
    image(i+580,j,1)=(100-((i+j)/2))/100;
    end
end

for i = 1:100
    for j = 1:100
    image(i+680,j,3)=((i+j)/2)/100;
    image(i+680,j,1)=(((i+j)/2))/100;
    end
end

for i = 1:100
    for j = 1:100
    image(i+680,j+100,2)=((i+j)/2)/100;
    image(i+680,j+100,1)=(((i+j)/2))/100;
    end
end

for i = 1:100
    for j = 1:100
    image(i+680,j+200,2)=((i+j)/2)/100;
    image(i+680,j+200,3)=(((i+j)/2))/100;
    end
end

%red block with contrasts
image(1:399,431:830,1)=1;
image(100:150,531:581,1)=0;
image(200:250,531:581,1:3)=1;
image(100:150,631:681,2)=1;
image(100:150,631:681,1)=0;
image(200:250,631:681,3)=1;
image(200:250,631:681,1)=0;

%green block with contrasts
image(1:399,831:1230,2)=1;
image(100:150,931:981,2)=0;
image(200:250,931:981,1:3)=1;
image(100:150,1031:1081,1)=1;
image(100:150,1031:1081,2)=0;
image(200:250,1031:1081,3)=1;
image(200:250,1031:1081,2)=0;

%blue block with contrasts
image(1:399,1231:1630,3)=1;
image(100:150,1331:1381,3)=0;
image(200:250,1331:1381,1:3)=1;
image(100:150,1431:1481,1)=1;
image(100:150,1431:1481,3)=0;
image(200:250,1431:1481,2)=1;
image(200:250,1431:1481,3)=0;


%white block with contrasts
image(1:399,1631:1944,:)=1;
image(100:150,1731:1781,:)=0;
image(200:250,1731:1781,1:2)=0;
image(100:150,1831:1881,2:3)=0;
image(100:150,1831:1881,3)=0;
image(100:150,1831:1881,2)=0;
image(200:250,1831:1881,1:2:3)=0;

%show image
figure, imshow(image)

for i = 1:1944
    for j= 1:2592
        if mod(j,2) == 0 
            if mod(i,2) == 0 
                image_out(i,j) = image(i,j,2)*4095;
            else
                image_out(i,j) = image(i,j,3)*4095;
            end
        else
            if mod(i,2) == 0 
                image_out(i,j) = image(i,j,1)*4095; 
            else
                image_out(i,j) = image(i,j,2)*4095;
            end    
        end
    end
end

fid=fopen('bilde.txt', 'wt');
for i = 1:1944
    for j= 1:2592
        fprintf(fid, '%d\n', image_out(i,j));
    end
end
fclose(fid);
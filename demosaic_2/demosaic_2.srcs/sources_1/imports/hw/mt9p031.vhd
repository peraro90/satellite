library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity mt9p031 is
	port(
		OE, TRIGGER             : in  std_logic;
		EXTCLK                  : in  std_logic;
		PIXCLK                  : buffer std_logic;
		DOUT                    : out std_logic_vector(11 downto 0);
		FRAME_VALID, LINE_VALID : out std_logic;
		RESET_BAR               : in  std_logic
	);
end entity mt9p031;

library ieee;
use ieee.numeric_std.all;
use work.memread.all;
architecture BEH of mt9p031 is
	constant columnPadStart : integer := 3; -- number of cycles before LV
	constant columnPadEnd   : integer := 3; -- cycles after LV
	constant rowPadStart    : integer := 3; -- cycles before FV
	constant rowPadEnd      : integer := 3; -- cycles after FV
	constant activex        : integer := 10;
	constant activey        : integer := 10;
begin
	clock_output : process(EXTCLK) is   -- model propagation delay
	begin
		if EXTCLK'event then
			if OE = '0' then
				PIXCLK <= transport EXTCLK after 17.7 ns;
			else
				PIXCLK <= 'Z';
			end if;
		end if;
	end process clock_output;

	readout : process is
		variable array_data : sensor_array := initialize_2d("arraydata.dat", activex, activey);
	begin
		if RESET_BAR = '0' then
		else
			loop
				FRAME_VALID <= '0';
				LINE_VALID  <= '0';
				wait until rising_edge(TRIGGER);
				-- top vertical blanking area
				for i in 1 to (columnPadStart + activex + columnPadEnd) * rowPadStart loop
					wait until falling_edge(PIXCLK);
					DOUT        <= x"FFF";
					FRAME_VALID <= '1';
				end loop;

				for y in 0 to activey - 1 loop
					for blank in 1 to columnPadStart loop -- horizontal blanking left
						wait until falling_edge(PIXCLK);
						DOUT <= x"FFF";
					end loop;

					for x in 0 to activex - 1 loop -- active area
						wait until falling_edge(PIXCLK);
						LINE_VALID <= '1';
						DOUT       <= array_data(x, y);
					end loop;

					for blank in 1 to columnPadEnd loop -- horizontal blanking right
						wait until falling_edge(PIXCLK);
						DOUT       <= x"FFF";
						LINE_VALID <= '0';
					end loop;
				end loop;
				-- bottom vertical blanking area
				for i in 1 to (columnPadStart + activex + columnPadEnd) * rowPadEnd loop
					wait until falling_edge(PIXCLK);
					DOUT <= x"FFF";
				end loop;
				wait until falling_edge(PIXCLK);
				FRAME_VALID <= '0';

			end loop;
		end if;
	end process readout;

end architecture BEH;

library ieee;
use ieee.numeric_std.all;
use work.memread.all;
architecture RTL of mt9p031 is
	constant columnPadStart : integer := 3; -- number of cycles before LV
	constant columnPadEnd   : integer := 3; -- cycles after LV
	constant rowPadStart    : integer := 3; -- cycles before FV
	constant rowPadEnd      : integer := 3; -- cycles after FV
	constant activex        : integer := 10;
	constant activey        : integer := 10;
begin
	PIXCLK <= EXTCLK;

	readout : process (EXTCLK) is
		variable y : integer range 0 to 3 := 0;
		variable x : integer range 0 to 3 := 0;
	begin
		if falling_edge(EXTCLK) then
			if TRIGGER = '1' then
				DOUT <= std_logic_vector(to_unsigned(x, 6)) & std_logic_vector(to_unsigned(x, 6));
				if y < 3 then
					if x < 3 then
						x := x+1;
					else
						y := y+1;
						x := 0;
					end if;
				else
					y := 0;
					x := 0;
				end if;
				if y > 0 and y < 3 then
					FRAME_VALID <= '1';
				else
					FRAME_VALID <= '0';
				end if;
				
				if x > 0 and x < 3 then
					LINE_VALID <= '1';
				else
					LINE_VALID <= '0';
				end if;
				
			else
				DOUT <= x"AAA";
				FRAME_VALID <= '0';
				LINE_VALID <= '0';
				y := 0;
				x := 0;
			end if;
		end if;
	end process readout;

end architecture RTL;


----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:06:58 03/14/2016 
-- Design Name: 
-- Module Name:    maxmin - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity minmax is
	port(
		clk : in std_logic;
		rst : in std_logic;
		R : in std_logic_vector(15 downto 0);
		G_0 : in std_logic_vector(15 downto 0);
		G_1 : in std_logic_vector(15 downto 0);
		B : in std_logic_vector(15 downto 0);
		maxR : out std_logic_vector(15 downto 0);
		maxG_0: out std_logic_vector(15 downto 0);
		maxG_1 : out std_logic_vector(15 downto 0);
		maxB : out std_logic_vector(15 downto 0);
		minR : out std_logic_vector(15 downto 0);
		minG_0 : out std_logic_vector(15 downto 0);
		minG_1 : out std_logic_vector(15 downto 0);
		minB : out std_logic_vector(15 downto 0)
	);
end entity minmax;

architecture RTL of minmax is
	--signals goes here
	signal maxR_t : std_logic_vector(15 downto 0):= "0000000000000000";
	signal maxG_0_t : std_logic_vector(15 downto 0):= "0000000000000000";
	signal maxG_1_t : std_logic_vector(15 downto 0):= "0000000000000000";
	signal maxB_t : std_logic_vector(15 downto 0):= "0000000000000000";
	signal minR_t : std_logic_vector(15 downto 0):= "1111111111111111";
	signal minG_0_t : std_logic_vector(15 downto 0):= "1111111111111111";
	signal minG_1_t : std_logic_vector(15 downto 0):= "1111111111111111";
	signal minB_t : std_logic_vector(15 downto 0):= "1111111111111111";
	signal max_temp : std_logic_vector(15 downto 0):= "0000000000000000";   
	signal min_temp : std_logic_vector(15 downto 0):= "1111111111111111";
	begin
		maximum : process(clk, rst) is
		begin
			
			
				if R > maxR_t then
					maxR <= R;
					maxR_t <= R;
				end if;
				if B > maxB_t then
					maxB <= B;
					maxB_t <= B;
				end if;
				if G_0 > maxG_0_t then
					maxG_0 <= G_0;
					maxG_0_t <= G_0;
				end if;
				if G_1 > maxG_1_t then
					maxG_1 <= G_1;	
					maxG_1_t <= G_1;
				end if;	
			end process maximum;
			
						
		minimum : process(clk,rst) is
			begin
				if R < minR_t then
					minR <= R;	
					minR_t <= R;
				end if;
				if B < minB_t then
					minB <= B;
					minB_t <= B;
				end if;
				if G_0 < minG_0_t then
					minG_0 <= G_0;
					minG_0_t <= G_0;
				end if;
				if G_1 < minG_1_t then
					minG_1 <= G_1;
					minG_1_t <= G_1;
				end if;
				
			end process minimum;
	end architecture;		

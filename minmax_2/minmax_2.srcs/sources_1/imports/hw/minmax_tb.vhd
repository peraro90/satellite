--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:51:51 05/22/2016
-- Design Name:   
-- Module Name:   C:/Users/Public/vhdl/mixed/hw/minmax_tb.vhd
-- Project Name:  camera
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: minmax
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY minmax_tb IS
END minmax_tb;
 
ARCHITECTURE behavior OF minmax_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT minmax
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         R : IN  std_logic_vector(15 downto 0);
         G : IN  std_logic_vector(15 downto 0);
         B : IN  std_logic_vector(15 downto 0);
         maxR : OUT  std_logic_vector(15 downto 0);
         maxG : OUT  std_logic_vector(15 downto 0);
         maxB : OUT  std_logic_vector(15 downto 0);
         minR : OUT  std_logic_vector(15 downto 0);
         minG : OUT  std_logic_vector(15 downto 0);
         minB : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal R : std_logic_vector(15 downto 0);
   signal G : std_logic_vector(15 downto 0);
   signal B : std_logic_vector(15 downto 0);

 	--Outputs
   signal maxR : std_logic_vector(15 downto 0);
   signal maxG : std_logic_vector(15 downto 0);
   signal maxB : std_logic_vector(15 downto 0);
   signal minR : std_logic_vector(15 downto 0);
   signal minG : std_logic_vector(15 downto 0);
   signal minB : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: minmax PORT MAP (
          clk => clk,
          rst => rst,
          R => R,
          G => G,
          B => B,
          maxR => maxR,
          maxG => maxG,
          maxB => maxB,
          minR => minR,
          minG => minG,
          minB => minB
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      rst <= '1';
      wait for 100 ns;
      rst <= '0';	
      R <= "0000000001100000";
      G <= "0000000011000000";
      B <= "0000000001100000";
      wait for clk_period;
      R <= "0000000000001000";
      G <= "0000000100000000";
      B <= "0000000000100000";
      wait for clk_period;
      R <= "0000000000000111";
      G <= "0000001110001100";
      B <= "0000000000110000";
      wait for clk_period;
      R <= "0001100000100011";
      G <= "0000000111000000";
      B <= "0000100000000000";
      wait for clk_period;
      R <= "0000011100000000";
      G <= "0000000001111000";
      B <= "0000011000000000";
      wait for clk_period;
      R <= "0000000000000000";
      G <= "0000000011000000";
      B <= "0011000000000000";
      wait for clk_period;

      -- insert stimulus here 

      wait;
   end process;

END;

i=1;
j=1;
image = zeros(2592,1944,3);
%show image
figure, imshow(image)

for i = 1:2592
    for j= 1:1944
        if mod(j,2) == 0 
            if mod(i,2) == 0 
                image_out(i,j) = image(i,j,2)*4095;
            else
                image_out(i,j) = image(i,j,3)*4095;
            end
        else
            if mod(i,2) == 0 
                image_out(i,j) = image(i,j,1)*4095; 
            else
                image_out(i,j) = image(i,j,2)*4095;
            end    
        end
    end
end

fid=fopen('bilde.txt', 'wt');
for i = 1:2592
    for j= 1:1944
        fprintf(fid, '%d\n', image_out(i,j));
    end
end
fclose(fid);
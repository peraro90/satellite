library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.std_logic_textio.all;
package memread is
	type sensor_array is array (15 downto 0, 15 downto 0) of std_logic_vector(11 downto 0);
	function to_slv(tmp : string) return std_logic_vector;
	function initialize_2d(filename : string; x : natural; y : natural) return sensor_array;
end package memread;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
package body memread is
	function to_slv(tmp : string) return std_logic_vector is
		variable temp  : std_logic_vector(11 downto 0);
		variable digit : natural range 0 to 15;
	begin
		for i in 1 to 3 loop
			case tmp(i) is
				when '0' to '9' => digit := Character'pos(tmp(i)) - Character'pos('0');
				when 'A' to 'F' => digit := 10 + Character'pos(tmp(i)) - Character'pos('A');
				when others     => digit := 0;
			end case;
			temp((4 - i) * 4 - 1 downto (3 - i) * 4) := std_logic_vector(to_unsigned(digit, 4));
		end loop;
		return temp;
	end function;

	function initialize_2d(filename : string; x : natural; y : natural) return sensor_array is
		file file_ptr : text;
		variable temp                 : sensor_array;
		variable current_line         : line;
		variable current_string       : string(1 to 3);
		variable current_x, current_y : natural := 0;
	-- other variables
	begin
		file_open(file_ptr, filename, READ_MODE);
		while not endfile(file_ptr) loop
			readline(file_ptr, current_line);
			read(current_line, current_string);
			temp(current_x, current_y) := to_slv(string(current_string));
			current_x  := current_x + 1;
			if (current_x >= x) then
				current_x := 0;
				current_y := current_y +1;
			end if;
			if (current_y >= y) then
				current_y := 0;
				exit; -- last line done
			end if;

		end loop;
		file_close(file_ptr);
		return temp;
	end function;
end package body memread;



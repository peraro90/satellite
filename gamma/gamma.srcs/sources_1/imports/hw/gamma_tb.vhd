--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:52:20 05/22/2016
-- Design Name:   
-- Module Name:   C:/Users/Public/vhdl/mixed/hw/gamma_tb.vhd
-- Project Name:  camera
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: gamma
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY gamma_tb IS
END gamma_tb;
 
ARCHITECTURE behavior OF gamma_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT gamma
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         X : IN unsigned(15 downto 0);
         Xg : OUT  unsigned(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal X : unsigned(15 downto 0) := (others => '0');

 	--Outputs
   signal Xg : unsigned(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: gamma PORT MAP (
          clk => clk,
          rst => rst,
          X => X,
          Xg => Xg
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      rst <= '1';
      wait for 100 ns;	
      X <= "0011001100110011";
      wait for clk_period;
      X <= "0000000000000010";
      wait for clk_period;
      X <= "0000000000000100";
      wait for clk_period;
      X <= "0000000000001000";
      wait for clk_period;
      X <= "0000000000001001";
      wait for clk_period;
      X <= "0000001010101000";
      wait for clk_period;
      X <= "0011001111110011";
      wait for clk_period;
      X <= "0011001100000010";
      wait for clk_period;
      X <= "0000000011110100";
      wait for clk_period;
      X <= "0011100001001000";
      wait for clk_period;
      X <= "0000000100001001";
      wait for clk_period;
      X <= "0000001010101000";
      wait for clk_period;
      X <= "0000000000111111";
      wait for clk_period;
      X <= "0000001111111111";
      wait for clk_period;
      X <= "0000000000011101";
      wait for clk_period;
      X <= "0000000001111100";
      wait for clk_period;
      X <= "0000000000001111";
      wait for clk_period;
      X <= "0000001011111000";
      wait for clk_period;
      -- insert stimulus here 

      wait;
   end process;

END;

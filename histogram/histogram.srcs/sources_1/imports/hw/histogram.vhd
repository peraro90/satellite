----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Per Arne R�nning
-- 
-- Create Date:    12:28:19 03/15/2016 
-- Design Name: 
-- Module Name:    histogram - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

entity histogram is
	port(
		clk : in std_logic;
		rst : in std_logic;
		red: in std_logic_vector (15 downto 0);
		blue: in std_logic_vector (15 downto 0);
		green: in std_logic_vector (15 downto 0);
		red_out: out integer;
		blue_out: out integer;
		green_out: out integer
		
	);
end entity;

		

	architecture Behavioral of histogram is
	-- signals goes here
	type colour_mem is array (7 downto 0) of std_logic_vector(7 downto 0);
	
	signal red_amount : colour_mem;
	signal blue_amount : colour_mem;
	signal green_amount : colour_mem;
	signal red_test : std_logic_vector(2 downto 0);
	
	begin
	
		sorting : process (clk, rst, red, blue, green) is
		
		variable red_temp : std_logic_vector(2 downto 0);
		variable blue_temp : std_logic_vector(2 downto 0);
		variable green_temp : std_logic_vector(2 downto 0);

		begin
		  if rst='1' then
		      red_amount <= (others => "00000000");
		      blue_amount <= (others => "00000000");
		      green_amount <= (others => "00000000");
		  elsif rising_edge(clk) then
		            red_temp := red(15 downto 13);
		            blue_temp := blue(15 downto 13);
		            green_temp := green(15 downto 13);
					
					red_test <= red_temp;
					
					red_amount(to_integer(unsigned(red_temp))) <= red_amount(to_integer(unsigned(red_temp)))+1;
					blue_amount(to_integer(unsigned(blue_temp))) <= blue_amount(to_integer(unsigned(blue_temp)))+1;
					green_amount(to_integer(unsigned(green_temp))) <= green_amount(to_integer(unsigned(green_temp)))+1;
            end if;
		end process;
	end architecture;



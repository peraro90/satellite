----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Per Arne R�nning
-- 
-- Create Date:    12:28:19 03/15/2016 
-- Design Name: 
-- Module Name:    histogram - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity histogram is
	port(
		clk : in std_logic;
		rst : in std_logic;
		sensor_extended : in std_logic_vector (15 downto 0);
		colour : in std_logic_vector (1 downto 0) --To mark what color that are read into the module
		
	);
end entity;

		

	architecture Behavioral of histogram is
	-- signals goes here
	type colour_array is array (7 downto 0) of std_ulogic;
	type colour_mem is array (7 downto 0) of colour_array;
	
	signal red_amount : colour_mem;
	signal blue_amount : colour_mem;
	signal green_1_amount : colour_mem;
	signal green_2_amount : colour_mem;
	
	function  divide  (a : UNSIGNED; b : UNSIGNED) return UNSIGNED is
		variable a1 : unsigned(a'length-1 downto 0):= a;
		variable b1 : unsigned(b'length-1 downto 0):= b;
		variable p1 : unsigned(b'length downto 0):= (others => '0');
		variable i : integer:=0;
		variable eight : std_logic_vector(15 downto 0) := "0000000000001000"; -- 16 bit long variable with a value of 8

		begin
			for i in 0 to b'length-1 loop
				p1(b'length-1 downto 1) := p1(b'length-2 downto 0);
				p1(0) := a1(a'length-1);
				a1(a'length-1 downto 1) := a1(a'length-2 downto 0);
				p1 := p1-b1;
				if(p1(b'length-1) ='1') then
					a1(0) :='0';
					p1 := p1+b1;
				else
					a1(0) :='1';
				end if;
			end loop;
			return a1;

		end divide;
	
	begin
	
		sorting : process (clk, rst, sensor_extended, colour) is
		begin
			case colour is 
				when "00" =>
					red_amount(divide(sensor_extended, eight))<= red_amount(divide(sensor_extended, eight)) + 1;
				when "01" =>
					blue_amount(divide(sensor_extended, eight))<= blue_amount(divide(sensor_extended, eight)) + 1;
				when "10" =>
					green_1_amount(divide(sensor_extended, eight))<= green_1_amount(divide(sensor_extended, eight))  + 1;
				when "11" =>
					green_2_amount(divide(sensor_extended, eight))<= green_2_amount(divide(sensor_extended, eight)) + 1;
			end case;
		end process;
	end architecture;



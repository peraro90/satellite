--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:40:30 04/19/2016
-- Design Name:   
-- Module Name:   C:/Users/Public/vhdl/mixed/hw/demosaic_testbench.vhd
-- Project Name:  camera
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: demosaic
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
USE ieee.numeric_std.all; 
--use ieee.math_real.all; 
--USE ieee.std_logic_arith.all;
LIBRARY std;
use std.textio.all;

 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY demosaic_testbench IS
END demosaic_testbench;
 
ARCHITECTURE behavior OF demosaic_testbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT demosaic
    PORT(
         clk : in  STD_LOGIC;
			rst : in  STD_LOGIC;
			FV : in STD_LOGIC;
			LV : in STD_LOGIC;
			camera_input: in STD_LOGIC_VECTOR(15 downto 0);
			red_out: out STD_LOGIC_VECTOR(15 downto 0);
			blue_out: out STD_LOGIC_VECTOR(15 downto 0);
			green_out: out STD_LOGIC_VECTOR(15 downto 0);
			i_out: out integer;
			j_out: out integer
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '1';
	signal FV : std_logic := '0';
	signal LV :	std_logic := '0';
   signal camera_input : std_logic_vector(15 downto 0) := (others => '0');
	signal red_out : std_logic_vector(15 downto 0) := (others => '0');
	signal blue_out : std_logic_vector(15 downto 0) := (others => '0');
	signal green_out : std_logic_vector(15 downto 0) := (others => '0');
    signal i_out : integer := 0;
	signal j_out : integer := 0;
	--period of clock,bit for indicating end of file.
	signal endoffile : bit := '0';
		
	--data read from the file.
	signal    dataread_red : real;
    signal    dataread_blue : real;
	signal    dataread_green : real;
	--data to be saved into the output file.
	signal    datatosave : real;
	--line number of the file read or written.
	signal    linenumber : integer:=1;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: demosaic PORT MAP (
          clk => clk,
          rst => rst,
			 FV => FV,
			 LV => LV,
          camera_input => camera_input,
			 red_out => red_out,
			 blue_out => blue_out,
			 green_out => green_out,
			 i_out => i_out,
			 j_out => j_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
   
	signal_process :process
	begin
	wait until clk = '0';
		FV <= '1';
		rst<= '0';
	end process;
	
	
	reading :
process
    file   infile    : text is in  "C:\Users\Public\vhdl\demosaic_4\bilde.txt";   --declare input file
    variable  inline    : line; --line number declaration
    variable  dataread1    : real;
	 variable  status : boolean;
begin
wait until clk = '1';

if endoffile = '1' then
    LV <= '0';
else
    LV <= '1';
end if;

if (not endfile(infile)) then   --checking the "END OF FILE" is not reached.
readline(infile, inline);       --reading a line from the file.
  --reading the data from the line and putting it in a real type variable.
read(inline, dataread1);
camera_input <= std_logic_vector(to_unsigned(INTEGER(dataread1),16));   --put the value available in variable in a signal.
else
endoffile <='1';         --set signal to tell end of file read file is reached.
end if;

end process reading;

writing :
process
    file      outfile_red  : text is out "C:\Users\Public\vhdl\demosaic_4\bilde_out_red.txt";  --declare output file
    file      outfile_blue  : text is out "C:\Users\Public\vhdl\demosaic_4\bilde_out_blue.txt";  --declare output file
    file      outfile_green  : text is out "C:\Users\Public\vhdl\demosaic_4\bilde_out_green.txt";  --declare output file
    variable  outline_red : line;   --line number declaration  
    variable  outline_blue  : line;   --line number declaration  
    variable  outline_green  : line;   --line number declaration  
begin
wait until (clk = '0') and (i_out = 1);

dataread_red <= real(to_integer(unsigned(red_out)));
dataread_blue <= real(to_integer(unsigned(blue_out)));
dataread_green <= real(to_integer(unsigned(green_out)));
if(i_out = 1) then   --if the file end is not reached.
--write(linenumber,value(real type),justified(side),field(width),digits(natural));
write(outline_red, dataread_red, right, 16, 12);
write(outline_blue, dataread_green, right, 16, 12);
write(outline_green, dataread_blue, right, 16, 12);
-- write line to external file.
writeline(outfile_red, outline_red);
writeline(outfile_green, outline_blue);
writeline(outfile_blue, outline_green);
linenumber <= linenumber + 1;
else
null;
end if;

end process writing;
end architecture;
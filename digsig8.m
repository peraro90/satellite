gamma = [1.25 -0.5 0 0];

matrix1 = gamma(1);
matrix2 = [gamma(1) gamma(2); gamma(2) gamma(1)];
matrix3 = [gamma(1) gamma(2) gamma(3); gamma(2) gamma(1) gamma(2); gamma(3) gamma(2) gamma(1)];

a_1 = -gamma(2)/gamma(1);
sigma1 = (sum([1 a_1] .* gamma(1:2)));
a_2 = -gamma(2:3)/matrix2;
sigma2 = sum([1 a_2] .* gamma(1:3));
a_3 = -gamma(2:4)/matrix3;
sigma3 = sum([1 a_3] .* gamma(1:4));



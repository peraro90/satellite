----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:18:15 04/14/2016 
-- Design Name: 
-- Module Name:    demosaic - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity demosaic is
    Port ( 	clk : in  STD_LOGIC;
			rst : in  STD_LOGIC;
			FV : in STD_LOGIC;
			LV : in STD_LOGIC;
			camera_input: in STD_LOGIC_VECTOR(15 downto 0);
			red_out: out STD_LOGIC_VECTOR(15 downto 0);
			blue_out: out STD_LOGIC_VECTOR(15 downto 0);
			green_out: out STD_LOGIC_VECTOR(15 downto 0);
			i_out: out integer;
			j_out: out integer
			);
end demosaic;

architecture Behavioral of demosaic is
--signals goes here

	type camera_width is array (2591 downto 0) of std_logic_vector(15 downto 0);
	type camera_area is array (2591 downto 0, 4 downto 0) of std_logic_vector(15 downto 0);
	signal green_layer : camera_area := ((others => (others => (others => '0'))));
	signal red_layer : camera_area := ((others => (others => (others => '0'))));
	signal blue_layer : camera_area := ((others => (others => (others => '0'))));
	signal camera_values : camera_area := ((others => (others => (others => '0'))));
	signal i,j, cam_x, out_x, out_i, rb_x, rb_i : integer := 0;
	
	signal sc : std_logic := '0';
	signal camera_ready, read_out_ready, layer_ready : std_logic := '0';
	signal layer_x : integer;
begin

counter: process(clk,rst)
begin
	if (rst = '1') then
	    camera_ready <= '0';
	    read_out_ready <= '0';
	    cam_x <= 0;
	    out_x <= 0;
	    layer_x <= 0;
	    rb_x <= 0;
	    rb_i <= 0;
	    
		i <= 0;
		j <= 0;
	elsif (rising_edge(clk)) then
		if (i < 2591) then
			i <= i+1;
		elsif (j < 1943) then
			j <= j+1;
			i <= 0;
			cam_x <= (j mod 5);
		else
			i <= 0;
			j <= 0;
		end if;
		-- counter for red and blue, 4 steps behind the rest
		if i = 4 then
		  rb_i <= 0;
		  rb_x <= layer_x;
		else
	      rb_i <= rb_i + 1;
	    end if;
	    -- read out counter needs to be behind this
	    if i = 6 then
	       read_out_ready <= '1';
	       out_i <= 0;
	       out_x <= layer_x;
	    else
	       out_i <= out_i + 1;
	    end if; 
	    
	    if j > 3 then
            layer_ready <= '1';
            layer_x <= ((j-3) mod 5);
        end if;
        
	end if;
end process;


read_in: process(clk, rst, FV, LV)
begin
    if (LV = '1') and (rising_edge(clk))  then
        camera_values(i,cam_x) <= camera_input;
    end if;
end process;

read_out: process(clk,rst)
variable read_out_counter : integer;
begin
    if (rst = '1') then
        read_out_counter := 0;
        i_out <= 0;
        red_out   <= (others => '0');
        green_out <= (others => '0');
        blue_out  <= (others => '0');
    elsif (read_out_ready = '1') and (rising_edge(clk)) and (read_out_counter < 5038848) then
        i_out <= 1;
        read_out_counter := read_out_counter + 1;
		red_out   <= red_layer(out_i,out_x);
        green_out <= green_layer(out_i,out_x);
        blue_out  <= blue_layer(out_i,out_x);
    else
        i_out <= 0;
    end if;
end process;

layering: process(clk, rst)
	variable blue_temp, red_temp, green_temp : signed(15 downto 0);
	variable rb_up, rb_down, layer_up, layer_down : integer;
begin
	if (rst = '1') then

		-- Not initializing the large arrays: camera_values, red_layer, green_layer, blue_layer.
		-- Resetting those here would probably result in synthesis tools interpreting them as 80M flip-flops each. Not nice.
	elsif (rising_edge(clk)) and (layer_ready = '1') then
	   if (layer_x = 4) then
	       layer_up := 0;
	       layer_down := layer_x-1;
	   elsif (layer_x = 0) then
	       layer_down := 4;
	       layer_up := layer_x+1;
	   else
	       layer_up := layer_x+1;
	       layer_down := layer_x-1;
	   end if;
	   
	   if (rb_x = 4) then
	       rb_up := 0;
	       rb_down := rb_x-1;
	   elsif (rb_x = 0) then
	       rb_up := rb_x+1;
	       rb_down := 4;
	   end if;
				if (i = 0) or (i = 2591) or (j = 0) or (j = 1943) then
					-- doing nothing on the edges
					-- green_layer(i,j), blue_layer(i,j) and red_layer(i,j) will stay at initial values (0)
				else
					if (j mod 2) = 0 then
						--j is even
						if(i mod 2) = 0 then
							-- i is even
					      green_layer(i,layer_x) <= camera_values(i,layer_x); --even,even
						else
							--i is odd
	                  blue_layer(i,layer_x)<= camera_values(i,layer_x); --odd, even
	                  --estimate green
	                  green_temp := signed(camera_values(i,(layer_up))+ camera_values(i,(layer_up))+ camera_values((i+1),layer_x)+ camera_values((i-1),layer_x));
	                  green_layer(i,layer_x) <= std_logic_vector(shift_right(green_temp,2));
	                  --estimate red
	                  red_temp := signed(camera_values((i+1),(layer_up))+ camera_values((i-1),(layer_down))+ camera_values((i+1),(layer_down))+ camera_values((i-1),(layer_up)));
	                  red_layer(i,layer_x) <= std_logic_vector(shift_right(red_temp,2));
						end if;
					else
						--j is odd
						if (i mod 2) = 0 then
							-- i is even
							red_layer(i,layer_x) <= camera_values(i,layer_x); --even, odd
							--estimate green
							green_temp := signed(camera_values(i,(layer_up))+ camera_values(i,(layer_down))+ camera_values((i+1),layer_x)+ camera_values((i-1),layer_x));
							green_layer(i,layer_x) <= std_logic_vector(shift_right(green_temp,2));
							--estimate blue
	                  blue_temp := signed(camera_values((i+1),(layer_up))+ camera_values((i-1),(layer_down))+ camera_values((i+1),(layer_down))+ camera_values((i-1),(layer_up)));
	                  blue_layer(i,layer_x)<= std_logic_vector(shift_right(blue_temp,2));
						else
							-- i is odd
							green_layer(i,layer_x) <= camera_values(i,layer_x); --odd, odd
						end if;
					end if;
				end if;
				
			--when red_blue_case =>

				if (i = 0) or (i = 2591) or (j = 0) or (j = 1943) then
					-- doing nothing on the edges
					-- green_layer(i,j), blue_layer(i,j) and red_layer(i,j) will stay at initial values (0)
				else
					if(rb_x mod 2) = 0 then
						if(rb_i mod 2) = 0 then
							--estimate blue
							blue_temp := signed(blue_layer(rb_i,(rb_up))+ blue_layer(rb_i,(rb_down))+ blue_layer((rb_up),rb_x)+ blue_layer((rb_down),rb_x));
							blue_layer(rb_i,rb_x) <= std_logic_vector(shift_right(blue_temp,2));
							--estimate red
							red_temp:= signed(red_layer(rb_i,(rb_up))+ red_layer(rb_i,(rb_down))+ red_layer((rb_up),rb_x)+ red_layer((rb_down),rb_x));
							red_layer(rb_i,rb_x) <= std_logic_vector(shift_right(red_temp,2));
						else
							-- nothing needs to be done here
						end if;
					else
						if(rb_i mod 2) = 0 then
							--nothing needs to be done here
						else
							blue_temp:= signed (blue_layer(rb_i,(rb_up))+ blue_layer(rb_i,(rb_down))+ blue_layer((rb_up),rb_x)+ blue_layer((rb_down),rb_x));
							blue_layer(rb_i,rb_x) <= std_logic_vector(shift_right(blue_temp,2));
							
							red_temp:= signed (red_layer(rb_i,(rb_up))+ red_layer(rb_i,(rb_down))+ red_layer((rb_up),rb_x)+ red_layer((rb_down),rb_x));
							red_layer(rb_i,rb_x) <= std_logic_vector(shift_right(red_temp,2));
						end if;
					end if;		
				end if;
   end if;
end process;

end Behavioral;


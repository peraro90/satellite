----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:18:15 04/14/2016 
-- Design Name: 
-- Module Name:    demosaic - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity demosaic is
    Port ( 	clk : in  STD_LOGIC;
			rst : in  STD_LOGIC;
			FV : in STD_LOGIC;
			LV : in STD_LOGIC;
			camera_input: in STD_LOGIC_VECTOR(15 downto 0);
			red_out: out STD_LOGIC_VECTOR(15 downto 0);
			blue_out: out STD_LOGIC_VECTOR(15 downto 0);
			green_out: out STD_LOGIC_VECTOR(15 downto 0);
			i_out: out integer;
			j_out: out integer
			);
end demosaic;

architecture Behavioral of demosaic is
--signals goes here

	type camera_width is array (2591 downto 0) of std_logic_vector(15 downto 0);
	type camera_area is array (2591 downto 0, 1943 downto 0) of std_logic_vector(15 downto 0);
	signal green_layer : camera_area := ((others => (others => (others => '0'))));
	signal red_layer : camera_area := ((others => (others => (others => '0'))));
	signal blue_layer : camera_area := ((others => (others => (others => '0'))));
	signal camera_values : camera_area := ((others => (others => (others => '0'))));
	signal i,j : integer := 0;
	signal sc : std_logic := '0';
	
	type state_type is (init, read_in, read_out, normal_case, special_case, red_blue_case);
	signal current_state, next_state : state_type;
begin

state_changer: process(clk,rst)
begin
	if (rst = '1') then
		current_state <= init;
		i <= 0;
		j <= 0;
	elsif (rising_edge(clk)) then
		current_state <= next_state;
		if (i < 2591) then
			i <= i+1;
		elsif (j < 1943) then
			j <= j+1;
			i <= 0;
		else
			i <= 0;
			j <= 0;
		end if;
	end if;
end process;

nstate: process(current_state, FV, LV, i, j) -- non clocked process, companion to 'state_changer'
begin
	case (current_state) is
		when init =>
		if (FV = '1') then
			if (LV = '1') then
				next_state <= read_in;
			else
				next_state <= init;
			end if;
		else
			next_state <= init;
		end if;
		
		when read_in =>
			if (i = 2591) and (j = 1943) then
				next_state <= normal_case;
			else
				next_state <= read_in;
			end if;
			
		when read_out =>
			if (i = 2591) and (j = 1943) then
				next_state <= init;
			else
				next_state <= read_out;
			end if;
			
		when normal_case =>
			if (i = 2591) and (j = 1943) then
				next_state <= red_blue_case;
			else
				next_state <= normal_case;
			end if;
			
		when red_blue_case =>
			if (i = 2591) and (j = 1943) then
				next_state <= special_case;
			else
				next_state <= red_blue_case;
			end if;
			
		when special_case =>
   		next_state <= read_out; 
	end case;	
end process;



layering: process(clk, rst)
	variable blue_temp, red_temp, green_temp : signed(15 downto 0);
begin
	if (rst = '1') then
		red_out   <= (others => '0');
		green_out <= (others => '0');
		blue_out  <= (others => '0');
		i_out     <= 0;
		-- Not initializing the large arrays: camera_values, red_layer, green_layer, blue_layer.
		-- Resetting those here would probably result in synthesis tools interpreting them as 80M flip-flops each. Not nice.
	elsif (rising_edge(clk)) then
		case (current_state) is
			when init =>
				red_out   <= (others => '0');
				green_out <= (others => '0');
				blue_out  <= (others => '0');

			when read_in =>
				camera_values(i,j) <= camera_input;
				
			when read_out =>
				red_out   <= red_layer(i,j);
				green_out <= green_layer(i,j);
				blue_out  <= blue_layer(i,j);
	
		   	i_out <= 1;
				if (i = 2591) then
					if (j = 1943) then
						i_out <= 0;
					end if;
				end if;

			when normal_case =>
				if (i = 0) or (i = 2591) or (j = 0) or (j = 1943) then
					-- doing nothing on the edges
					-- green_layer(i,j), blue_layer(i,j) and red_layer(i,j) will stay at initial values (0)
				else
					if (j mod 2) = 0 then
						--j is even
						if(i mod 2) = 0 then
							-- i is even
					      green_layer(i,j) <= camera_values(i,j); --even,even
						else
							--i is odd
	                  blue_layer(i,j)<= camera_values(i,j); --odd, even
	                  --estimate green
	                  green_temp := signed(camera_values(i,(j+1))+ camera_values(i,(j-1))+ camera_values((i+1),j)+ camera_values((i-1),j));
	                  green_layer(i,j) <= std_logic_vector(shift_right(green_temp,2));
	                  --estimate red
	                  red_temp := signed(camera_values((i+1),(j+1))+ camera_values((i-1),(j-1))+ camera_values((i+1),(j-1))+ camera_values((i-1),(j+1)));
	                  red_layer(i,j) <= std_logic_vector(shift_right(red_temp,2));
						end if;
					else
						--j is odd
						if (i mod 2) = 0 then
							-- i is even
							red_layer(i,j) <= camera_values(i,j); --even, odd
							--estimate green
							green_temp := signed(camera_values(i,(j+1))+ camera_values(i,(j-1))+ camera_values((i+1),j)+ camera_values((i-1),j));
							green_layer(i,j) <= std_logic_vector(shift_right(green_temp,2));
							--estimate blue
	                  blue_temp := signed(camera_values((i+1),(j+1))+ camera_values((i-1),(j-1))+ camera_values((i+1),(j-1))+ camera_values((i-1),(j+1)));
	                  blue_layer(i,j)<= std_logic_vector(shift_right(blue_temp,2));
						else
							-- i is odd
							green_layer(i,j) <= camera_values(i,j); --odd, odd
						end if;
					end if;
				end if;
				
			when red_blue_case =>
				if (i = 0) or (i = 2591) or (j = 0) or (j = 1943) then
					-- doing nothing on the edges
					-- green_layer(i,j), blue_layer(i,j) and red_layer(i,j) will stay at initial values (0)
				else
					if(j mod 2) = 0 then
						if(i mod 2) = 0 then
							--estimate blue
							blue_temp := signed(blue_layer(i,(j+1))+ blue_layer(i,(j-1))+ blue_layer((i+1),j)+ blue_layer((i-1),j));
							blue_layer(i,j) <= std_logic_vector(shift_right(blue_temp,2));
							--estimate red
							red_temp:= signed(red_layer(i,(j+1))+ red_layer(i,(j-1))+ red_layer((i+1),j)+ red_layer((i-1),j));
							red_layer(i,j) <= std_logic_vector(shift_right(red_temp,2));
						else
							-- nothing needs to be done here
						end if;
					else
						if(i mod 2) = 0 then
							--nothing needs to be done here
						else
							blue_temp:= signed (blue_layer(i,(j+1))+ blue_layer(i,(j-1))+ blue_layer((i+1),j)+ blue_layer((i-1),j));
							blue_layer(i,j) <= std_logic_vector(shift_right(blue_temp,2));
							
							red_temp:= signed (red_layer(i,(j+1))+ red_layer(i,(j-1))+ red_layer((i+1),j)+ red_layer((i-1),j));
							red_layer(i,j) <= std_logic_vector(shift_right(red_temp,2));
						end if;
					end if;		
				end if;
				
			when special_case =>
			if i = 0 then
		      if (j mod 2) = 0 then
		          green_layer(i,j) <= camera_values(i,j);
		          red_layer(i,j) <= red_layer(i+1,j);
		          blue_layer(i,j) <= blue_layer(i+1,j);
		      else
		          green_layer(i,j) <= camera_values(i+1,j);
		          red_layer(i,j) <= camera_values(i,j);
		          blue_layer(i,j) <= blue_layer(i+1,j);
		      end if;
		   elsif i = 1943 then
		      if (j mod 2) = 0 then
		          green_layer(i,j) <= camera_values(i-1,j);
		          blue_layer(i,j) <= camera_values(i,j);
		          red_layer(i,j) <= red_layer(i-1,j);
		      else
		          green_layer(i,j) <= camera_values(i,j);
		          blue_layer(i,j) <= blue_layer(i-1,j);
		          red_layer(i,j) <= red_layer(i-1,j);
		      end if;
		   elsif j = 0 then
		      if (i mod 2) = 0 then
		      	  blue_layer(i,j) <= blue_layer(i,j+1);
                  red_layer(i,j) <= red_layer(i,j+1);
		          green_layer(i,j) <= camera_values(i,j);
		      else
		          green_layer(i,j) <= camera_values(i,j+1);
		          blue_layer(i,j) <= camera_values(i,j);
		          red_layer(i,j) <= red_layer(i,j+1);
		      end if;
		   elsif j = 2591 then
		      if (i mod 2) = 0 then
		          green_layer(i,j) <= camera_values(i,j-1);
		          red_layer(i,j) <= camera_values(i,j);
		          blue_layer(i,j) <= blue_layer(i,j-1);
		      else
		          green_layer(i,j) <= camera_values(i,j);
		          blue_layer(i,j) <= blue_layer(i,j-1);
                  red_layer(i,j) <= red_layer(i,j-1);
		      end if;
		   end if;
		end case;	
   end if;
end process;

end Behavioral;


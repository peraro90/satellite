-- TestBench Template 

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;

  ENTITY testbench IS
  END testbench;

  ARCHITECTURE behavior OF testbench IS 

  -- Component Declaration
          COMPONENT histogram
          PORT(
                  clk : in std_logic;
                  rst : in std_logic;
                  red: in std_logic_vector (15 downto 0);
                  blue: in std_logic_vector (15 downto 0);
                  green: in std_logic_vector (15 downto 0);
                  red_out: out integer;
                  blue_out: out integer;
                  green_out: out integer
                  );
          END COMPONENT;

          SIGNAL clk :  std_logic;
          SIGNAL rst :  std_logic;
          SIGNAL red :  std_logic_vector(15 downto 0);
          SIGNAL blue :  std_logic_vector(15 downto 0);
          SIGNAL green :  std_logic_vector(15 downto 0);
          SIGNAL red_out : integer;
          SIGNAL blue_out : integer;
          SIGNAL green_out : integer;
          
           constant clk_period : time := 10 ns;

  BEGIN

  -- Component Instantiation
          uut: histogram PORT MAP(
                  clk => clk,
                  rst => rst,
                  red => red,
                  blue => blue,
                  green => green,
                  red_out => red_out,
                  blue_out => blue_out,
                  green_out => green_out
          );
          
        clk_process :process
          begin
               clk <= '0';
               wait for clk_period/2;
               clk <= '1';
               wait for clk_period/2;
          end process;

  --  Test Bench Statements
     tb : PROCESS
     BEGIN
     

        
        rst <= '1';
        wait for 100 ns; -- wait until global set/reset completes
        rst <= '0';
        red <= "1111111111111111";
        blue <= "0000000000001111";
        green <= "0000000000001111";
        -- Add user defined stimulus here

        wait for clk_period ;   -- will wait forever
        red <= "1111000000001111";
        blue <= "0000000011111111";
        green <= "0000111100001100";
        
        wait for clk_period ;   -- will wait forever
        red <= "1111000000111111";
        blue <= "0000000011011101";
        green <= "0000111100001111";
        wait for clk_period ;   -- will wait forever
        red <= "1111000110001111";
        blue <= "0000001111111000";
        green <= "0000100100001111";
        
     END PROCESS tb;
  --  End Test Bench 

  END;

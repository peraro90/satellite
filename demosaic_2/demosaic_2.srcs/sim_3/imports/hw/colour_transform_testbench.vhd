--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:07:42 04/29/2016
-- Design Name:   
-- Module Name:   C:/Users/Public/vhdl/mixed/hw/colour_transform_testbench.vhd
-- Project Name:  camera
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: colour_transform
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY colour_transform_testbench IS
END colour_transform_testbench;
 
ARCHITECTURE behavior OF colour_transform_testbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT colour_transform
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         R : IN  std_logic_vector(11 downto 0);
         G : IN  std_logic_vector(11 downto 0);
         B : IN  std_logic_vector(11 downto 0);
         Y : OUT  std_logic_vector(11 downto 0);
         Db : OUT  std_logic_vector(11 downto 0);
         Dr : OUT  std_logic_vector(11 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal R : std_logic_vector(11 downto 0) := (others => '0');
   signal G : std_logic_vector(11 downto 0) := (others => '0');
   signal B : std_logic_vector(11 downto 0) := (others => '0');

 	--Outputs
   signal Y : std_logic_vector(11 downto 0);
   signal Db : std_logic_vector(11 downto 0);
   signal Dr : std_logic_vector(11 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: colour_transform PORT MAP (
          clk => clk,
          rst => rst,
          R => R,
          G => G,
          B => B,
          Y => Y,
          Db => Db,
          Dr => Dr
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 
		
		R <= "111100001111";
		G <= "111100001111";
		B <= "111100001111";
		wait for 100 ns;
		R <= "111111111111";
		G <= "111100000000";
		B <= "000000001111";
		wait for 100 ns;
		R <= "101010101010";
		G <= "010101010101";
		B <= "001100110011";
		wait for 100 ns;
		

      wait;
   end process;

END;

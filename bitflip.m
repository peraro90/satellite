% les inn data , bit for bit
filename = 'earthrise.2400.jpg';
fid = fopen(filename, 'rb');
img = fread(fid, inf, 'ubit1');
fclose(fid);

% antall bitflips :
flips=20;

%Lag 10 bilder med random bitflips :
for i=1:10
    img_flipped=img;
    % find random position to do the flips
    rand_index=randi(length(img),1,flips);
    %do the flips using xor 1
    img_flipped(rand_index)=bitxor(img_flipped(rand_index),1) ;

    % skriv data til nye bilder
    out_file=[filename(1:end -4) , '_flipped_', int2str(i) , filename(end -3:end)];
    fid = fopen(out_file,'wb');
    fwrite(fid,img_flipped,'ubit1');
    fclose(fid);
end
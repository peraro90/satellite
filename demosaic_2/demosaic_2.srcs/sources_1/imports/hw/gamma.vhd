----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:30:02 03/14/2016 
-- Design Name: 
-- Module Name:    gamma - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gamma  is
	port(
		clk : in std_logic;
		rst : in std_logic;
		X : in unsigned(15 downto 0);
		Xg : out unsigned(15 downto 0)
	);
end entity gamma; 

architecture RTL of gamma is
begin	  
	



sqrt: process (clk, rst) is	  

variable a : unsigned(31 downto 0):=(others => '0');  --original input.
variable q : unsigned(15 downto 0):=(others => '0');  --result.
variable left,right,r : unsigned(17 downto 0):=(others => '0');  --input to adder/sub.r-remainder.
variable i : integer:=0;
begin	
	
a(15 downto 0) := X;
a(31 downto 16) := "0000000000000000";

q:=(others => '0');
left:=(others => '0');
right:=(others => '0');
r:=(others => '0');

for i in 0 to 15 loop
 
right(0):='1';
right(1):=r(17);
right(17 downto 2):=q;
left(1 downto 0):=a(31 downto 30);
left(17 downto 2):=r(15 downto 0);
a(31 downto 2):=a(29 downto 0);  --shifting by 2 bit.
if ( r(17) = '1') then
r := left + right;
else
r := left - right;
end if;
q(15 downto 1) := q(14 downto 0);
q(0) := not r(17);
end loop; 
Xg <= q;

end process;	
	end architecture;


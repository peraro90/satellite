fileID1 = fopen('bilde_out_red.txt','r');
fileID2 = fopen('bilde_out_blue.txt','r');
fileID3 = fopen('bilde_out_green.txt','r');
formatSpecs = '%f';
A = fscanf(fileID1,formatSpecs);
B = fscanf(fileID2,formatSpecs);
C = fscanf(fileID3,formatSpecs);
n = 1;
picture_red(1:1944,1:2592) = 0;
picture_blue(1:1944,1:2592) = 0;
picture_green(1:1944,1:2592) = 0;
image = zeros(1944,2592,3);

for i = 1:1943
    for j= 1:2592
        picture_red(i,j) = A(n)/4095;
        image(i,j,1) = A(n)/4095;
        picture_blue(i,j) = B(n)/4095;
        image(i,j,3) = B(n)/4095;
        picture_green(i,j) = C(n)/4095;
        image(i,j,2) = C(n)/4095;
        n = n+1;
    end
end

figure, imshow(image)
figure, imshow(picture_red)
figure, imshow(picture_green)
figure, imshow(picture_blue)




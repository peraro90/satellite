----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:36:57 04/04/2016 
-- Design Name: 
-- Module Name:    colour_transform - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity colour_transform is
	port(
		clk : in std_logic;
		rst : in std_logic;
		R : in std_logic_vector(15 downto 0);
		G : in std_logic_vector(15 downto 0);
		B : in std_logic_vector(15 downto 0);
		Y : out std_logic_vector(15 downto 0);
		Db : out std_logic_vector(15 downto 0);
		Dr : out std_logic_vector(15 downto 0)
	);
end entity colour_transform;
	
architecture RTL of colour_transform is 
	--signals goes here
	begin

		transform : process (clk, rst) is
		variable Y_temp : signed(15 downto 0);
			begin
			Db <= B - G;
			Dr <= R - G;
			Y_temp := signed(R + B + G + G);
			Y <= std_logic_vector(shift_right(Y_temp,2));
			end process;
	end architecture;
	

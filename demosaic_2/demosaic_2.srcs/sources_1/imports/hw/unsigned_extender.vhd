----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:50:18 03/14/2016 
-- Design Name: 
-- Module Name:    unsigned_extender - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity unsigned_extender is
	port(
		clk : in std_logic;
		rst : in std_logic;
		sensor_data : in std_logic_vector(11 downto 0);
		sensor_extended : out std_logic_vector(15 downto 0)
	);
end entity;
	
architecture RTL of unsigned_extender is 
	--signals goes here
	begin
		extend : process (clk, rst, sensor_data, sensor_extended) is
		begin
				sensor_extended <= std_logic_vector(resize(unsigned(sensor_data), 16));
	end process;
end architecture;
